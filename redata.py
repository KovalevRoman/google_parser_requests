import psycopg2
from settings import settings
import re

def get_regdata(marker):
    try:
        pg_conn = psycopg2.connect("dbname='{database}' user='{user}' host='{host}' "
                                   "password='{password}' port='{port}'".format(**settings.PG_SETTINGS))
    except:
        print("I am unable to connect to the database")

    pg_cursor = pg_conn.cursor()
    rdata = ''

    try:
        pg_cursor.execute("BEGIN WORK")
        pg_cursor.execute("LOCK TABLE google.links IN ACCESS EXCLUSIVE MODE")
        pg_cursor.execute("""SELECT link FROM google.links WHERE status='Not processed' LIMIT 1""")
        rdata = pg_cursor.fetchone()[0]
        rdata_sub = rdata
        if re.search('\'', rdata):
            rdata_sub = re.sub('\'', '\'\'', rdata)
        pg_cursor.execute("UPDATE google.links SET status = '{}' WHERE link = '{}'".format('Pending', rdata_sub))
        pg_cursor.execute("COMMIT WORK")
    except Exception as e:
        pass
        print ("Exception(): {}".format(e))
    pg_conn.commit()
    pg_conn.close()
    return rdata

def put_result(rdata, result):
    try:
        if re.search('\'', rdata):
            rdata = re.sub('\'', '\'\'', rdata)
        pg_conn = psycopg2.connect("dbname='{database}' user='{user}' host='{host}' "
                                   "password='{password}' port='{port}'".format(**settings.PG_SETTINGS))

        pg_cursor = pg_conn.cursor()
        pg_cursor.execute("BEGIN WORK")
        pg_cursor.execute("LOCK TABLE google.links IN ACCESS EXCLUSIVE MODE")
    except Exception as e:
        print("Exception occured(Cannot connect to database: {}".format(e))

    if result == "Null":
        try:
            # print("Result: ", result, "type: ", type(result))
            # print("Rdata: ", rdata, "type: ", type(rdata))

            pg_cursor.execute("UPDATE google.links SET status = 'Not processed' WHERE link = '{}'".format(rdata))
            pg_cursor.execute("COMMIT WORK")
            pg_conn.commit()
            pg_conn.close()
            return True
        except Exception as e:
            print("Exception is occured (put_error_result)", e)
            return False
    else:
        try:
            # print("Result: ", result, "type: ", type(result))
            # print("Rdata: ", rdata, "type: ", type(rdata))
            if result != 'Null':
                pg_cursor.execute("UPDATE google.links SET result = '{}', status = 'Processed' WHERE link = '{}'".format(
                    result, rdata))
                pg_cursor.execute("COMMIT WORK")
                pg_conn.commit()
                pg_conn.close()
            return True
        except Exception as e:
            print("Exception is occured (put_good_result)", e)
            return False

def put_db(rdata, results):
    try:
        pg_conn = psycopg2.connect("dbname='{database}' user='{user}' host='{host}' "
                                   "password='{password}' port='{port}'".format(**settings.PG_SETTINGS))

        pg_cursor = pg_conn.cursor()
        pg_cursor.execute("""UPDATE google.links SET result={} WHERE link={}""").format(results, rdata)
        pg_conn.commit()
        return True
    except:
        print("I am unable to connect to the database")
        return False

