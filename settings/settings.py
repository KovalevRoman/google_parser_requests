import os
from sys import platform

REQUIRED_ACCS = 500000

THREADS = 60

PG_SETTINGS = {
    'user': 'postgres',
    'password': '123',
    'database': 'google',
    'host': '127.0.0.1',
    'port': '5432',
    'table': 'links'
}



# File with user-agents. User-agent on each line.
MOBILE_UA_LIST_FILE = 'assets/useragents_mobile.txt'
DESKTOP_UA_LIST_FILE = 'assets/useragents_desktop.txt'
UA_TYPE = 'Mobile' #or Desktop or Mobile

IS_PROXY_ACCESS = True


BASE_PATH = os.path.join(os.path.dirname(__file__), '..')

if __name__ == '__main__':
    main()