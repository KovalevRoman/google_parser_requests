from redata import *
import random
import requests
from bs4 import BeautifulSoup
# import csvv


def navigate():
    proxyservers = ['37.48.118.90:13042',
             '83.149.70.159:13042'
             ]
    proxy = random.choice(proxyservers)
    ua = 'Mozilla/5.0 (Linux; U; Tizen 2.0; en-us) AppleWebKit/537.1 (KHTML, like Gecko) Mobile TizenBrowser/2.0'
    user_agent = {
        'User-Agent':ua}
    base_url = 'https://www.google.ru/search?q=intitle: '
    rdata = get_regdata(marker='google')
    url = base_url + rdata
    # print(rdata)
    # print(url)
    # print(user_agent)
    # print(proxy)

    try:
        r = requests.get(url, proxies={'http': proxy, 'https': proxy}, headers=user_agent)
        flag = True
    except Exception as e:
        print("Exception occured (navigate()-1):{}".format(e))
        result = 'Null'
        put_result(rdata, result)
        flag = False

    if flag == True:
        try:
            soup = BeautifulSoup(r.text, 'html.parser')
            # print(soup)
            results = soup.find('div', id="resultStats")
            if results != None:
                results = results.get_text()
                put_result(rdata, results)
            else:
                raise Exception('Results empty (none)')
            print(results)
        except Exception as e:
            print("Exception is occured:{}".format(e))
            results = 'Null'
            put_result(rdata, results)

    '''
    # print(r.text)

    
    # try:
        
        r = requests.get('https://www.hashemian.com/whoami/',
                         proxies={'http': '37.48.118.90:13042', 'https': '37.48.118.90:13042'})
        soup = BeautifulSoup(r.text, 'html.parser')
        ip = soup.find_all('span')[4].get_text()
        print(ip)
        print(url)
    '''

def main():
    pass

if __name__ == '__main__':
    main()
