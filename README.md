# Консольный скрипт для парсинга количества упоминаний ключевой фразы в поисковике Google.com #

### Используемые технологии ###
* Python 3
* [Beautifulsoup4 4.6.0] (https://pypi.python.org/pypi/beautifulsoup4)
* [Python Requests library 2.18.4] (http://docs.python-requests.org/en/master/)
* База данных Postgres и адаптер [Psycopg2] (http://initd.org/psycopg/docs/)
* Python threading

### Features ###
* Многопоточность и возможность выбора количества потоков
* Поддержка подмены user-agent.
* Работа с прокси-серверами
* Вывод информации о выполнении программы в консоль
* Протестированная скорость парсинга: 100 000 фраз/120 часов на 40 прокси-серверах (3 rotating back-connect from http://stormproxies.com)



Контакты: 

Email: rvk.sft[_at_]gmail.com