import psycopg2
from settings import settings
import re

def make(list):
    slist = []
    for i in list:
        if i[-1] == '\n':
            i = i[:-1]
        slist.append(i)
    return slist

def sendtodb(pg_conn, list):
    pg_cursor = pg_conn.cursor()
    for i in list:
        if re.search('\'', i):
            i = re.sub('\'', '\'\'', i)
        pg_cursor.execute(
            "INSERT INTO google.links (link) VALUES ('{}')".format(i))
    pg_conn.commit()
    return True


def main():
    links = open('google.txt', 'r')
    mlinks = make(links)
    # for i in mlinks:
    #     print(i)

    try:
        pg_conn = psycopg2.connect("dbname='{database}' user='{user}' host='{host}' "
                                   "password='{password}' port='{port}'".format(**settings.PG_SETTINGS))
    except:
        print ("I am unable to connect to the database")

    sendtodb(pg_conn, mlinks)

    pg_conn.close()



if __name__ == '__main__':
    main()
