import random
import threading
from time import sleep
from settings import settings
import google

class Thread_launcher (threading.Thread):
    def __init__(self, name, ua, thread_quantity,lock):
        threading.Thread.__init__(self)
        self.name = name
        self.ua = ua
        self.threads = threads
        self.thread_quantity = thread_quantity
        self.lock = lock

    def run(self):
        global counter

        # print ("Starting thread " + self.name)
        threads.append(self.name)
        google.navigate()
        # print(counter)
        # print ("Exiting thread and deleting " + self.name)
        self.lock.acquire()
        try:
            # Удаляем отработавший трид
            threads.pop(threads.index(self.name))
            # Счетчик запущенных тридов. Увеличивть при успешной отправке (то есть перенести в др место в коде(
            counter += 1
        finally:
            self.lock.release()  # освободить блокировку, что бы ни произошло
        if counter <= settings.REQUIRED_ACCS-settings.THREADS:
            if len(self.threads) <= self.thread_quantity: # Если тридов меньше, чем требуется в ТРЕАД
                tname = random.randint(100000, 999999) # Имя нового треда
                start_thread(tname, lock, counter) # старт нового треда
        else:
            if counter == settings.REQUIRED_ACCS: # Заканчиваем работу, если послали все мессаги
                print(counter, "accounts were registered. Check it in database.")
            else: # Последние 5 мессат (не обязательо
                print("Process is endings...")


def init_thread_namelist(threadList):
    for i in range(settings.THREADS):
        threadList.append(random.randint(100000, 999999))
    return (threadList)


def start_thread(tName, lock, counter):
    # ua = random.choice(ualist)
    sleep(random.randint(1,3))
    ua = 'Mozilla/5.0 (Windows NT 6.3; rv:36.0) Gecko/20100101 Firefox/36.04'
    thread = Thread_launcher(tName, ua, settings.THREADS, lock)
    thread.start()


def init_first_thread_pack(threadList, lock):
    for tName in threadList:
        sleep(1)
        start_thread(tName, lock, counter)


def get_ua_list():
    if settings.UA_TYPE == 'Mobile':
        f = open(settings.MOBILE_UA_LIST_FILE, 'r')
    if settings.UA_TYPE == 'Desktop':
        f = open(settings.DESKTOP_UA_LIST_FILE, 'r')
    ualist = [ ]
    for i in f:
        i = i.replace('\n', '')
        ualist.append(i)
    return ualist


def main():
    global threads, ualist, proxylist, lock, counter
    counter = 0
    threads = []
    threadList = []
    lock = threading.Lock()
    # ualist = get_ua_list()
    if settings.REQUIRED_ACCS >= settings.THREADS:
        threadList = init_thread_namelist(threadList)
        init_first_thread_pack(threadList, lock)

if __name__ == '__main__':
    main()